package com.guniweb.spotshot;

import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Toast;

import com.etsy.android.grid.StaggeredGridView;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ui.ParseLoginBuilder;

public class MainActivity extends Activity implements
		NavigationDrawerFragment.NavigationDrawerCallbacks,
		AbsListView.OnItemClickListener {

	private static final int SAMPLE_DATA_ITEM_COUNT = 12;

	/**
	 * Fragment managing the behaviors, interactions and presentation of the
	 * navigation drawer.
	 */
	private NavigationDrawerFragment mNavigationDrawerFragment;

	/**
	 * StaggeredGridView configurations
	 */
	private StaggeredGridView mGridView;
	private DataAdapter mAdapter;

	/**
	 * Used to store the last screen title. For use in
	 * {@link #restoreActionBar()}.
	 */
	private CharSequence mTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager()
				.findFragmentById(R.id.navigation_drawer);
		mTitle = getTitle();

		// Set up the drawer.
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));

		// Set up the GridView
		mGridView = (StaggeredGridView) findViewById(R.id.grid_view);

		this.generateSampleData();
		
		/*ParseFacebookUtils.logIn(Arrays.asList("email", "user_friends", "user_birthday", "user_hometown"), this, new LogInCallback() {
			  @Override
			  public void done(ParseUser user, ParseException err) {
			    if (user == null) {
			      Log.d("MyApp", "Uh oh. The user cancelled the Facebook login.");
			    } else if (user.isNew()) {
			      Log.d("MyApp", "User signed up and logged in through Facebook!");
			    } else {
			      Log.d("MyApp", "User logged in through Facebook!");
			    }
			  }
			});
		*/
		
		ParseLoginBuilder builder = new ParseLoginBuilder(MainActivity.this);
		Intent parseLoginIntent = builder.setParseLoginEnabled(true)
			    .setParseLoginEnabled(true)
			    .setParseLoginButtonText("Login")
			    .setParseSignupButtonText("Registrieren")
			    .setParseLoginHelpText("Passwort vergessen?")
			    .setParseLoginInvalidCredentialsToastText("Die Kombination aus Email-Adresse und Passwort ist nicht korrekt. ")
			    .setParseLoginEmailAsUsername(true)
			    .setParseSignupSubmitButtonText("Registrieren")
			    .setFacebookLoginEnabled(true)
			    .setFacebookLoginButtonText("Facebook")
			    .build();
		startActivityForResult(builder.build(), 0);
		
		mGridView.setOnItemClickListener(this);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	  super.onActivityResult(requestCode, resultCode, data);
	  ParseFacebookUtils.finishAuthentication(requestCode, resultCode, data);
	}

	@Override
	public void onNavigationDrawerItemSelected(int position) {
		// update the main content by replacing fragments
		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager
				.beginTransaction()
				.replace(R.id.container,
						PlaceholderFragment.newInstance(position + 1)).commit();
	}

	public void onSectionAttached(int number) {
		switch (number) {
		case 1:
			mTitle = getString(R.string.title_section1);
			break;
		case 2:
			mTitle = getString(R.string.title_section2);
			break;
		case 3:
			mTitle = getString(R.string.title_section3);
			break;
		}
	}

	public void restoreActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(mTitle);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
			getMenuInflater().inflate(R.menu.main, menu);
			restoreActionBar();
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		private static final String ARG_SECTION_NUMBER = "section_number";

		/**
		 * Returns a new instance of this fragment for the given section number.
		 */
		public static PlaceholderFragment newInstance(int sectionNumber) {
			PlaceholderFragment fragment = new PlaceholderFragment();
			Bundle args = new Bundle();
			args.putInt(ARG_SECTION_NUMBER, sectionNumber);
			fragment.setArguments(args);
			return fragment;
		}

		public PlaceholderFragment() {
		}

		/*
		 * @Override public View onCreateView(LayoutInflater inflater, ViewGroup
		 * container, Bundle savedInstanceState) { View rootView =
		 * inflater.inflate(R.layout.fragment_main, container, false); return
		 * rootView; }
		 */

		@Override
		public void onAttach(Activity activity) {
			super.onAttach(activity);
			((MainActivity) activity).onSectionAttached(getArguments().getInt(
					ARG_SECTION_NUMBER));
		}
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view,
			int position, long id) {
		Toast.makeText(this, "Item Clicked: " + position, Toast.LENGTH_SHORT)
				.show();
	}

	public void generateSampleData() {

		final ArrayList<Data> datas = new ArrayList<Data>();

		ParseQuery<ParseObject> query = ParseQuery.getQuery("PartyPictures");
		query.findInBackground(new FindCallback<ParseObject>() {
		    public void done(List<ParseObject> partyPictures, ParseException e) {
		        if (e == null) {
		            Log.d("spotshot", "Retrieved " + partyPictures.size() + " pictures" + partyPictures.get(1).getString("userid"));
		            
		            int y = partyPictures.size();
		            
		            for (int i = 0; i < y; i++) {
		            	Log.d("spotshot", "userid " + partyPictures.get(i).getString("userid"));
		    			Data data = new Data();
		    			data.imageUrl = "http://www.party-screen.de/galleries/galleries/news/210683/image_newsgal_big/Echolot2_4835562__image_newsgal_big_.jpg";
		    			data.title = partyPictures.get(i).getString("userid");
		    			data.description = "Eine Beschreibung...";

		    			datas.add(data);
		    		}
		            
		        } else {
		            Log.d("spotshot", "Error: " + e.getMessage());
		        }
		    }

		});

		mAdapter = new DataAdapter(this, R.layout.list_item_sample,
				datas);
		mGridView.setAdapter(mAdapter);
	}

}
