package com.guniweb.spotshot.parseobject;

import com.parse.ParseFile;
import com.parse.ParseObject;

public class Picture extends ParseObject {
	
	public Picture() {
		
	}

	public ParseFile getPictureFile() {
		return getParseFile("pictureFile");
	}
	
	public void setPictureFile(ParseFile pictureFile) {
		put("pictureFile", pictureFile);
	}
	
	public String getDescription() {
		return getString("description");
	}
	
	public void setDescription(String description) {
		put("description", description);
	}
	
}
